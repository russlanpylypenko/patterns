<?php

class Singleton
{
    private static $instance;

    private static $configs = [];


    private function __construct()
    {
    }

    public static function getInstance()
    {
        if (empty(self::$instance)) {
            self::loadConfigs();
            self::$instance = new Singleton();
        }
        return self::$instance;
    }

    private static function loadConfigs()
    {
        if (file_exists('config.php')) {
            self::$configs = require_once 'config.php';
        }
    }

    public function getConfigs()
    {
        return self::$configs;
    }

    public function setConfig($key, $value)
    {
        self::$configs[$key] = $value;
    }

    public function getConfig($key)
    {
        try {
            if (!isset(self::$configs[$key])) {
               throw new Exception('Нет такого свойства');
            }
            return self::$configs[$key];
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }


}