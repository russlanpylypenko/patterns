<?php

require_once 'Singleton.php';


foreach (Singleton::getInstance()->getConfigs() as $key => $value) {
    echo $key . " => " . $value . '<br>';
}

echo "<hr>";

echo Singleton::getInstance()->getConfig('test');

Singleton::getInstance()->setConfig('test', '123');

echo "<hr>";

echo Singleton::getInstance()->getConfig('test');